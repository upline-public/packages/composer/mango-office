<?php

require_once dirname(__DIR__) . '/vendor/autoload.php';

use Dotenv\Dotenv;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\HttpFactory;
use Uplinestudio\MangoOffice\MangoCredentials;
use Uplinestudio\MangoOffice\MangoOfficeClient;
use Uplinestudio\MangoOffice\MangoOfficeService;

$dotenv = Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();

$creds = new MangoCredentials($_ENV['API_KEY'], $_ENV['SALT']);


$httpClient = new Client();
$httpFactory = new HttpFactory();
$mangoClient = new MangoOfficeClient(
    $httpClient,
    $httpFactory,
    $httpFactory,
    $creds
);

$mangoService = new MangoOfficeService($mangoClient);

try {
    var_dump($mangoService->getIncomingLines());
} catch (Throwable $exception) {
    var_dump($exception->getCode());
}