<?php

namespace Uplinestudio\MangoOffice;

use Uplinestudio\MangoOffice\Data\Line;
use Uplinestudio\MangoOffice\Data\RequestedStatistic;
use Uplinestudio\MangoOffice\Data\StatisticRow;
use Uplinestudio\MangoOffice\Filter\StatisticRequest;

class MangoOfficeService
{
    private MangoOfficeClient $client;

    public function __construct(MangoOfficeClient $client)
    {
        $this->client = $client;
    }

    /**
     * @return Line[]
     */
    public function getIncomingLines(): array
    {
        $data = $this->client->jsonRequest('/vpbx/incominglines');
        return array_map(function ($line) {
            return new Line($line);
        }, $data['lines']);
    }

    public function sendStatisticRequest(StatisticRequest $request): RequestedStatistic
    {
        $key = $this->client->jsonRequest('/vpbx/stats/request', $request->toArray())['key'];

        return new RequestedStatistic($key, $request->getFields());
    }

    /**
     * @param RequestedStatistic $requestedStatistic
     * @return StatisticRow[]
     */
    public function getStatisticResult(RequestedStatistic $requestedStatistic): array
    {
        $data = $this->client->request('/vpbx/stats/result', [
            'key' => $requestedStatistic->getKey()
        ]);

        $result = [];
        $rows = preg_split('/\r\n|\r|\n/', $data);
        foreach ($rows as $row) {
            if (!$row) {
                continue;
            }
            $line = explode(';', $row);
            $result[] = new StatisticRow(array_combine($requestedStatistic->getFields(), $line));
        }

        return $result;

    }

}