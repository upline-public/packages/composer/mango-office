<?php

namespace Uplinestudio\MangoOffice\Filter;

class StatisticRequest
{
    const FIELD_RECORDS = 'records';
    const FIELD_START = 'start';
    const FIELD_FINISH = 'finish';
    const FIELD_ANSWER = 'answer';
    const FIELD_FROM_EXTENSION = 'from_extension';
    const FIELD_FROM_NUMBER = 'from_number';
    const FIELD_TO_EXTENSION = 'to_extension';
    const FIELD_TO_NUMBER = 'to_number';
    const FIELD_DISCONNECT_REASON = 'disconnect_reason';
    const FIELD_ENTRY_ID = 'entry_id';
    const FIELD_LINE_NUMBER = 'line_number';
    const FIELD_LOCATION = 'location';
    const FIELD_CREATE = 'create';
    private int $timeStampFrom;
    private int $timeStampTo;
    private array $fields;



    public function __construct(int $timeStampFrom, int $timeStampTo, array $fields)
    {
        $this->timeStampFrom = $timeStampFrom;
        $this->timeStampTo = $timeStampTo;
        $this->fields = $fields;
    }

    /**
     * @return int
     */
    public function getTimeStampFrom(): int
    {
        return $this->timeStampFrom;
    }

    /**
     * @return int
     */
    public function getTimeStampTo(): int
    {
        return $this->timeStampTo;
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    public function toArray(): array
    {
        return [
            "date_from" => $this->timeStampFrom,
            "date_to" => $this->timeStampTo,
            "fields" => implode(',', $this->fields),
        ];
    }
}