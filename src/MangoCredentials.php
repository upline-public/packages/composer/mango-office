<?php

namespace Uplinestudio\MangoOffice;

class MangoCredentials
{
    private string $apiKey;
    private string $salt;

    public function __construct(string $apiKey, string $salt)
    {
        $this->apiKey = $apiKey;
        $this->salt = $salt;
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    /**
     * @return string
     */
    public function getSalt(): string
    {
        return $this->salt;
    }
}