<?php

namespace Uplinestudio\MangoOffice;

use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;

class MangoOfficeClient
{
    const API_DOMAIN = 'https://app.mango-office.ru';
    private ClientInterface $client;
    private RequestFactoryInterface $requestFactory;
    private StreamFactoryInterface $streamFactory;
    private MangoCredentials $mangoCredentials;

    public function __construct(
        ClientInterface         $client,
        RequestFactoryInterface $requestFactory,
        StreamFactoryInterface  $streamFactory,
        MangoCredentials        $mangoCredentials
    )
    {
        $this->client = $client;
        $this->requestFactory = $requestFactory;
        $this->streamFactory = $streamFactory;
        $this->mangoCredentials = $mangoCredentials;
    }

    public function request(string $address, array $data = [])
    {
        $request = $this->requestFactory
            ->createRequest('POST', self::API_DOMAIN . $address)
            ->withAddedHeader('Content-type', 'application/x-www-form-urlencoded')
            ->withBody($this->streamFactory->createStream(
                http_build_query($this->makeBody($data))
            ));

        $response = $this->client->sendRequest($request);
        if ($response->getStatusCode() !== 200) {
            throw new \RuntimeException('Wrong http answer', $response->getStatusCode());
        }
        return $response->getBody()->getContents();
    }

    public function jsonRequest(string $address, array $data = [])
    {
        return json_decode($this->request($address, $data), true);
    }

    private function makeBody(array $data): array
    {
        $json = json_encode($data, JSON_FORCE_OBJECT);
        return [
            'vpbx_api_key' => $this->mangoCredentials->getApiKey(),
            'sign' => $this->makeSignature($json),
            'json' => $json
        ];
    }

    private function makeSignature(string $json): string
    {
        return hash('sha256', $this->mangoCredentials->getApiKey() . $json . $this->mangoCredentials->getSalt());
    }
}