<?php

namespace Uplinestudio\MangoOffice\Data;

class RequestedStatistic
{
    private string $key;
    private array $fields;

    public function __construct(string $key, array $fields)
    {
        $this->key = $key;
        $this->fields = $fields;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }
}