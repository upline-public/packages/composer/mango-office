<?php

namespace Uplinestudio\MangoOffice\Data;

class Line
{
    private int $lineId;
    private ?string $name;
    private ?string $comment;
    private ?string $number;
    private ?string $region;
    private ?int $schema_id;
    private ?string $schema_name;

    public function __construct(array $data)
    {
        $this->lineId = $data['line_id'];
        $this->name = $data['name'] ?? null;
        $this->comment = $data['comment'] ?? null;
        $this->number = $data['number'] ?? null;
        $this->region = $data['region'] ?? null;
        $this->schema_id = $data['schema_id'] ?? null;
        $this->schema_name = $data['schema_name'] ?? null;
    }

    public function getLineId(): int
    {
        return $this->lineId;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function getSchemaId(): ?int
    {
        return $this->schema_id;
    }

    public function getSchemaName(): ?string
    {
        return $this->schema_name;
    }
}