<?php

namespace Uplinestudio\MangoOffice\Data;

/**
* @property-read string $records
* @property-read string $start
* @property-read string $finish
* @property-read string $answer
* @property-read string $from_extension
* @property-read string $from_number
* @property-read string $to_extension
* @property-read string $to_number
* @property-read string $disconnect_reason
* @property-read string $entry_id
* @property-read string $line_number
* @property-read string $location
* @property-read string $create
 */
class StatisticRow
{
    private array $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function __get($name)
    {
        return $this->data[$name] ?? null;
    }
}